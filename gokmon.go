// Copyright 2013, Carnegie Mellon University. All Rights Reserved.
// Use of this code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"flag"
)

// Command line arguments
var (
	serverAddr = flag.String("addr", "localhost:8080", "Server Address")
	runServer  = flag.Bool("serve", false, "Start server, instead of client")
	dbfile     = flag.String("db", "gokmon.db", "Datastore file for stats (server)")
)

func main() {
	flag.Parse()

	if *runServer {
		RunServerMain() // Runs for ever
	}
	RunClientMain() // Runs for ever
}
