// Copyright 2013, Carnegie Mellon University. All Rights Reserved.
// Use of this code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"sort"
	"sync"
	"time"
)

const (
	InvalidDataValue      = -999999
	saveQueueLength       = 1000
	expectedNodeCount     = 10 // How many nodes to expect in the cluster (soft cap)
	expectedStatTypeCount = 10 // How many stat-types to expect
)

// Type of each data point's value
type statvalue float32

// Type for timestamp values (nanoseconds since epoch)
type unixnano int64

// Type used by nodes to report stats
type StatRecord struct {
	Timestamp unixnano
	Nodename  string
	Stattype  string
	Statvalue statvalue
}

// Our data is stored in a dictionary as in this example
// data = {'sysload': {host1: []StatRecord,
//                     host2: []StatRecord}
//         'diskfree':{host1: []StatRecord}

// The following types define the nested maps, in in->out order
type hostrecordmap map[string][]StatRecord
type stattyperecordmap map[string]hostrecordmap

// Data store for all statistics
type StatStore struct {
	nodes     []string          // Nodes that have reported statistics
	stattypes []string          // Types of stats reported ( sysload, uptime, etc.)
	stats     stattyperecordmap // Data structure holding all values, described above
	stage     []StatRecord      // Staging area for node updates
	mutex     sync.RWMutex      // Mutex to operate on our data
	save      chan StatRecord   // Channel to save records to file
}

// Initialize a new stat store. 
func NewStatStore(filename string) *StatStore {
	s := &StatStore{
		nodes:     make([]string, 0, expectedNodeCount),
		stattypes: make([]string, 0, expectedStatTypeCount),
		stats:     make(stattyperecordmap),
		stage:     make([]StatRecord, 0, expectedNodeCount*expectedStatTypeCount),
	}

	if filename != "" {
		s.save = make(chan StatRecord, saveQueueLength)
		if err := s.load(filename); err != nil {
			log.Println("StatStore:", err)
		}
		go s.saveLoop(filename)
	}
	return s
}

// Add a record to statData
func (s *StatStore) add(r StatRecord) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	stattypedata, present := s.stats[r.Stattype]
	if !present {
		s.stattypes = append(s.stattypes, r.Stattype)
		stattypedata = make(hostrecordmap)
	}

	hostdata, present := stattypedata[r.Nodename]
	if !present {
		hostdata = make([]StatRecord, 1, 10000) // Estimate 10k data points
		hostdata[0] = r
	} else {
		hostdata = append(hostdata, r)
	}

	// Update nodes[] if new node is seen
	isUnseenNode := true
	for _, node := range s.nodes {
		if node == r.Nodename {
			isUnseenNode = false
			break
		}
	}
	if isUnseenNode {
		s.nodes = append(s.nodes, r.Nodename)
		sort.Strings(s.nodes)
	}

	stattypedata[r.Nodename] = hostdata
	s.stats[r.Stattype] = stattypedata
}

// Read database from file
func (s *StatStore) load(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	d := json.NewDecoder(f)
	for err == nil {
		var r StatRecord
		if err = d.Decode(&r); err == nil {
			s.add(r)
		}
	}
	if err == io.EOF {
		return nil
	}
	return err
}

// Save new values to file as they arrive
func (s *StatStore) saveLoop(filename string) {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal("StatStore:", err)
	}
	e := json.NewEncoder(f)
	for {
		r := <-s.save
		if err := e.Encode(r); err != nil {
			log.Println("StatStore:", err)
		}
	}
}

// Adds new load info into the "ping" list of loadDb. Return value is
// currently always nil.
func (s *StatStore) Put(r *StatRecord, status *bool) error {
	// Use our timestamp, override what came from nodes
	r.Timestamp = unixnano(time.Now().UnixNano())
	s.add(*r)
	s.save <- *r

	return nil

}
