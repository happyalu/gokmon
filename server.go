// Copyright 2013, Carnegie Mellon University. All Rights Reserved.
// Use of this code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/rpc"
	"strings"
	"text/template"
	"time"
)

var statstore *StatStore

// Log HTTP Requests in server
func makeLogHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

// Compressed output HTTP ResponseWriter
type gzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
	contentTypeDetected bool
}

func (w gzipResponseWriter) Write(b []byte) (int, error) {
	if !w.contentTypeDetected {
		if w.Header().Get("Content-Type") == "" {
			w.Header().Set("Content-Type", http.DetectContentType(b))
		}
		w.contentTypeDetected = true
	}
	return w.Writer.Write(b)
}

// Enable Compression of HTTP Responses
func makeGzipHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			handler.ServeHTTP(w, r)
			return
		}
		w.Header().Set("Content-Encoding", "gzip")
		w.Header().Set("Vary", "Accept-Encoding")
		gzw := gzip.NewWriter(w)
		defer gzw.Close()
		handler.ServeHTTP(gzipResponseWriter{Writer: gzw, ResponseWriter: w}, r)
	})
}

// Cache Control Handler
func makeMaxAgeHandler(seconds int, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Cache-Control", fmt.Sprintf("max-age=%d, public, must-revalidate, proxy-revalidate", seconds))
		h.ServeHTTP(w, r)
	})
}

// Run the HTTP Server
func RunServerMain() {
	log.Println("Starting Server")
	statstore = NewStatStore(*dbfile)
	rpc.RegisterName("StatStore", statstore)
	rpc.HandleHTTP()

	// We should handle /gokmon as our base path, but just in case...
	http.HandleFunc("/", ShowIndexPage)

	http.HandleFunc("/gokmon/", ShowIndexPage)
	http.HandleFunc("/gokmon/jsondata", ShowJSONData)
	http.Handle("/gokmon/res/", http.StripPrefix("/gokmon/res/", makeMaxAgeHandler(604800, http.FileServer(http.Dir("res")))))

	http.ListenAndServe(*serverAddr, makeGzipHandler(makeLogHandler(http.DefaultServeMux)))
}

func ShowIndexPage(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" && r.URL.Path != "/gokmon/" {
		// This is an invalid Page
		http.Error(w, "Page not Found", 404)
		return
	}

	t, err := template.ParseFiles("templates/highstockpage.html")
	if err != nil {
		// Serve the GViz Page
		ShowGVizPage(w, r)
		return
	}

	type PageData struct {
		StatTypes []string
		NodeNames []string
	}

	statstore.mutex.RLock()
	defer statstore.mutex.RUnlock()

	pd := &PageData{
		StatTypes: statstore.stattypes,
		NodeNames: statstore.nodes,
	}

	t.Execute(w, pd)
}

// Handler to show google visualization (default): no dependence on external files
func ShowGVizPage(w http.ResponseWriter, r *http.Request) {
	type PageData struct {
		StatTypes []string
		NodeNames []string
		DataRows  map[string]string
	}

	statstore.mutex.RLock()
	defer statstore.mutex.RUnlock()

	// Generate DataRows for the past day
	mintime := unixnano(time.Now().UnixNano() - int64(time.Hour*24))
	rows := make(map[string]string)
	numhosts := len(statstore.nodes)

	for _, stype := range statstore.stattypes {
		var str string
		str = ""

		for curhost := 0; curhost < numhosts; curhost++ {
			data := statstore.stats[stype][statstore.nodes[curhost]]
			for _, rec := range data {
				if rec.Timestamp < mintime {
					continue
				}
				str += fmt.Sprintf("[new Date(%d),", rec.Timestamp/1000000)

				for i := 0; i < numhosts; i++ {
					if curhost == i {
						str = fmt.Sprintf("%s %1.4f,", str, float32(rec.Statvalue))
					} else {
						str += " ,"
					}
				}
				str += "],\n"
			}
		}
		rows[stype] = str
	}

	pd := &PageData{
		StatTypes: statstore.stattypes,
		NodeNames: statstore.nodes,
		DataRows:  rows,
	}

	t, err := template.New("index").Parse(gviztemplate)
	if err != nil {
		log.Println("template:", err)
		http.Error(w, "Server Error", 500)
		return
	}

	t.Execute(w, pd)
}

// Handler to send JSON data of particular stat type/node
func ShowJSONData(w http.ResponseWriter, r *http.Request) {
	args := r.URL.Query()

	w.Header().Set("Content-Type", "text/javascript")

	s, ok1 := args["s"]
	n, ok2 := args["n"]
	c, ok3 := args["callback"]

	if !(ok1 && ok2 && ok3) {
		http.Error(w, "Invalid Request", 404)
		return
	}
	stattype := s[0]
	nodename := n[0]
	callback := c[0]

	statstore.mutex.RLock()
	defer statstore.mutex.RUnlock()

	records, ok := statstore.stats[stattype][nodename]
	if !ok {
		http.Error(w, "Data not Found", 404)
		return
	}

	mintime := unixnano(time.Now().UnixNano() - int64(time.Hour*24*7))

	fmt.Fprintf(w, "%s([", callback)
	for _, r := range records {
		if r.Timestamp < mintime {
			continue
		}
		fmt.Fprintf(w, "[%d,%1.4f],", r.Timestamp/1000000, r.Statvalue)
	}
	fmt.Fprintf(w, "]);")

}

// Template to serve our graphs
const gviztemplate = `
<html>
  <head>
    <title>goKmon: Monitor Your Nodes</title>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
	  google.load('visualization', '1.0', {'packages':['annotatedtimeline']});
	  google.setOnLoadCallback(drawCharts);

	  function drawCharts() {
	    {{range $index, $stattype := .StatTypes}}
	      drawChart_{{$index}}();
	    {{end}}
	  }

	  {{with $x := .}}
	  {{range $index, $stattype := $x.StatTypes}}
	    function drawChart_{{$index}}() {
	      var data = new google.visualization.DataTable();
	      data.addColumn('datetime', 'Time');
	      
	      {{range $nidx, $nodename := $x.NodeNames}}
	        data.addColumn('number', '{{$nodename}}');
	      {{end}}

	      data.addRows([
	        {{index $x.DataRows $stattype}}
	      ]);

	      new google.visualization.AnnotatedTimeLine(document.getElementById('chart_{{$index}}')).draw(data, {displayAnnotations: false, legendPosition: 'newRow'});
        }
	  {{end}}
	  {{end}}
    </script>
  </head>
  <body>
    <h1 style="text-align:center;">goKmon: Monitor Your Nodes</h1>
    
    {{range $index, $stattype := .StatTypes}}
	<div style="border-top:2px solid;">
	  <h3 style="text-align:center;">{{$stattype}}</h3>
      <div id="chart_{{$index}}"
           style="width: 800px; height: 300px; margin-left:auto;
                  margin-right:auto;">
		{{$stattype}}: No Data Available
      </div>
	  </div>
    {{end}}
  </body>
</html>

`
