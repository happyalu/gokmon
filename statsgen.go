// Copyright 2013, Carnegie Mellon University. All Rights Reserved.
// Use of this code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"
	"time"
)

// Return the average system load in the last minute. 
func getSystemLoadInfo() *StatRecord {
	var sysinfo syscall.Sysinfo_t

	var load float32

	// Use the system call to get system info
	err := syscall.Sysinfo(&sysinfo)
	if err != nil {
		log.Println("sysinfo: ", err)
		load = InvalidDataValue
	} else {
		// First value in Sysinfo_t.Loads is the per-minute value
		// But it needs divided by 65536 to get familiar load number
		load = float32(sysinfo.Loads[0]) / 65536.0
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.Println("hostname:", err)
		hostname = "unknown"
	}

	return &StatRecord{
		Timestamp: unixnano(time.Now().UnixNano()),
		Nodename:  hostname,
		Stattype:  "System Load",
		Statvalue: statvalue(load),
	}
}

// Returns the available disk space
func getFreeDiskSpaceInfo() *StatRecord {
	var freedisk float32
	// We use bash+df command for this. Not Portable.
	out, err := exec.Command("/bin/bash", "-c", "/bin/df -l -x tmpfs | awk  '{sum += $4} END{print sum}'").Output()
	if err != nil {
		log.Println("df:", err)
		freedisk = InvalidDataValue
	} else {
		_, err = fmt.Sscan(string(out[:]), &freedisk)
		if err != nil {
			freedisk = InvalidDataValue
		} else {
			freedisk *= 1000
		}
	}

	hostname, err := os.Hostname()
	if err != nil {
		log.Println("hostname:", err)
		hostname = "unknown"
	}

	return &StatRecord{
		Timestamp: unixnano(time.Now().UnixNano()),
		Nodename:  hostname,
		Stattype:  "Free Disk",
		Statvalue: statvalue(freedisk),
	}

}
