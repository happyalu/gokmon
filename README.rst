========
Go-K-Mon
========

Monitor your computer's information.

What is goKmon
==============

Go-K-Mon is a cluster monitoring tool written in Go.  A master node
runs goKmon in server mode (and hosts the goKmon webpage), and other
nodes run goKmon as clients that periodically send some information to
the server.

Currently this information is sent:
- system load (1-min average), every minute
- free disk space, every 20 minutes

Server saves these details and displays them via the web using graphs.

Usage
=====

server $ gokmon -serv -addr IP:PORT -db gokmon.db

client $ gokmon -addr serverIP:serverPORT
