// Copyright 2013, Carnegie Mellon University. All Rights Reserved.
// Use of this code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"io"
	"log"
	"net/rpc"
	"time"
)

const (
	maxFailedConnectAttempts = 30
)

func RunClientMain() {
	// We are running as a client
	var client *rpc.Client
	var err error
	client, err = rpc.DialHTTP("tcp", *serverAddr)
	if err != nil {
		log.Fatal("RPC: ", err)
	}

	sysloadcron := time.Tick(60 * time.Second)
	diskfreecron := time.Tick(1200 * time.Second)

	failedConnectAttempts := 0

	for {
		var status bool
		var rec *StatRecord

		select {
		case <-sysloadcron:
			rec = getSystemLoadInfo()
			log.Println("Sending Sysload Ping")
		case <-diskfreecron:
			rec = getFreeDiskSpaceInfo()
			log.Println("Sending Freedisk Ping")
		}

		if client == nil {
			client, err = rpc.DialHTTP("tcp", *serverAddr)
			if err != nil {
				failedConnectAttempts++
				log.Println("FAILED:", err)
				if failedConnectAttempts > maxFailedConnectAttempts {
					log.Fatal("Too many failed attempts to contact server. Aborting")
				}
				continue
			}
		}

		failedConnectAttempts = 0
		if err = client.Call("StatStore.Put", rec, &status); err != nil {
			switch err {
			case rpc.ErrShutdown:
				fallthrough
			case io.ErrUnexpectedEOF:
				client.Close()
				client = nil
			default:
				log.Fatal("FAILED:", err)
			}

		}
	}
}
